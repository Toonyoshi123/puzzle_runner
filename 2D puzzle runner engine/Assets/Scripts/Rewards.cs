﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Rewards : MonoBehaviour
{
    private WorldManager worldManager;

    List<Sprite> fruit = new List<Sprite>();
    [SerializeField]
    Image image;
    [SerializeField]
    Image FruitBasket;
    [SerializeField]
    TextMeshProUGUI text;
    public int rewardCount { get; private set; } = 0;

    Vector3 startingPosition;

    private void Start()
    {
        worldManager = FindObjectOfType<WorldManager>();
        startingPosition = image.rectTransform.position;
        fruit = worldManager.currentlyPlayedLevel.rewards;
        FruitBasket.sprite = worldManager.currentlyPlayedLevel.rewards[worldManager.currentlyPlayedLevel.rewards.Count -1];
    }

    public void IncreaseRewardCount()
    {
        // The animation.
        StartCoroutine("AnimateReward");
    }

    IEnumerator AnimateReward()
    {
        image.gameObject.SetActive(true);
        int randomInt = Random.Range(0,fruit.Count - 1);
        image.sprite = fruit[randomInt];
        float duration = 0;
        // Hover up and down for x seconds
        while (duration <= 2f)
        {
            var position = image.rectTransform.position;
            position.y = startingPosition.y + 50 * Mathf.Sin(Time.time*4);
            image.rectTransform.position = position;
            duration += Time.deltaTime;
            yield return null;
        }
        duration = 0;
        yield return duration;
        // Go to fruitBasket
        while (duration <= 2f)
        {
            image.transform.position = Vector3.Slerp(image.transform.position, FruitBasket.transform.position, Time.deltaTime);
            duration += Time.deltaTime;
            yield return null;
        }
        // Disappear
        image.gameObject.SetActive(false);
        rewardCount++;
        yield return rewardCount;
        text.text = rewardCount.ToString();
        yield return text.text;
        image.rectTransform.position = startingPosition;
        yield break;
    }
}
