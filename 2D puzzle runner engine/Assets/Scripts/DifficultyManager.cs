﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : MonoBehaviour
{
    private PuzzleManager puzzleManager;

    public int currentLevel { get; private set; } = 0;
    public int highestLevel { get; private set; } = 0;

    // Difficulty for the map.
    public List<int> amountOfPuzzles { get; private set; } = new List<int> {1,2,3};
    public List<float> maxDuration { get; private set; } = new List<float> {10,7.5f,5};


    // Difficulty for pop-all puzzle.
    public List<int> amountToSpawn { get; private set; } = new List<int> {3,4,6};


    // Difficulty for shake puzzle.
    public List<float> effectDistance { get; private set; } = new List<float> {0.1f,0.2f,0.3f};
    public List<int> amountOfParticles { get; private set; } = new List<int> {3,4,5};

    private void Start()
    {
        currentLevel = FindObjectOfType<WorldManager>().currentlyPlayedLevel.difficulty;
        puzzleManager = FindObjectOfType<PuzzleManager>();
    }


    public void InsertVariables(System.Type puzzleType, GameObject puzzleObject)
    {
        switch (puzzleType.ToString())
        {
            case "ChoicePuzzle":
                puzzleObject.GetComponent<ChoicePuzzle>().amountToSpawn = amountToSpawn[currentLevel];
                break;
            case "ShakePuzzle":
                puzzleObject.GetComponent<ShakePuzzle>().effectDistance = effectDistance[currentLevel];
                puzzleObject.GetComponent<ShakePuzzle>().amountOfParticles = amountOfParticles[currentLevel];
                break;
        }
        puzzleObject.GetComponent<Puzzle>().maxDuration = maxDuration[currentLevel];
    }

    public void ChangeLevel(bool madeNoMistakes)
    {
        if (currentLevel < 3 && madeNoMistakes)
        {
            currentLevel++;
            if (currentLevel >= highestLevel)
            {
                highestLevel = currentLevel;
            }
        }
        else if (currentLevel > 0 && !madeNoMistakes)
        {
            currentLevel--;
            if (currentLevel >= highestLevel)
            {
                highestLevel = currentLevel;
            }
        }
        var allPuzzles = FindObjectsOfType<Puzzle>();
        foreach (Puzzle puzzle in allPuzzles)
        {
            if (!puzzleManager.isInPuzzle)
            {
                puzzle.maxDuration = maxDuration[currentLevel];
                InsertVariables(puzzle.GetType(), puzzle.gameObject);
            }
        }
        puzzleManager.amountOfPuzzles = amountOfPuzzles[currentLevel];
    }
}
