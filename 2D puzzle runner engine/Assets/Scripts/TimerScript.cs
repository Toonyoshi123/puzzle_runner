﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerScript : MonoBehaviour
{
    private TextMeshProUGUI text;
    private float duration = 0f;
    public bool isRunning { get; set; } = false;

    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        if (isRunning)
        {
            duration -= Time.deltaTime;
        }
        text.text = "Time: " + System.Math.Round(duration,2);
        if (duration <= 0)
        {
            isRunning = false;
        }
    }

    public void StartTimer(float MaxDuration)
    {
        duration = MaxDuration;
        isRunning = true;
    }
}
