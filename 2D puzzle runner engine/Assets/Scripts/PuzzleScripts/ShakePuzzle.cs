﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakePuzzle : Puzzle
{
    private WaitForSeconds checkingInterval = new WaitForSeconds(0.1f);
    private Vector3 lastPosition;
    public float effectDistance = 0.1f;

    public float amountOfParticles = 5;
    private List<GameObject> particles = new List<GameObject>();
    private GameObject puzzleObject;

    private PuzzleManager puzzleManager;

    protected override void Start()
    {
        base.Start();
        puzzleManager = FindObjectOfType<PuzzleManager>();
        // Spawn puzzle object, and set lastPosition.
        puzzleObject = Instantiate(puzzleManager.MinigameObjects[0], transform.position + Vector3.up,Quaternion.identity);
        puzzleObject.SetActive(false);
        puzzleObject.transform.parent = transform;
        lastPosition = puzzleObject.transform.position;
        // Spawn all particles.
        for (int i = 0; i < amountOfParticles; i++)
        {
            SpawnParticles();
        }
    }

    private void SpawnParticles()
    {
        // Randomize position on top of puzzle object.
        var newParticle = Instantiate(puzzleManager.MinigameObjects[1],puzzleObject.transform);
        newParticle.transform.localScale *= 3;
        newParticle.transform.Translate(new Vector3(0.3f,0,0));
        int rand1 = Random.Range(0,1);
        int rand2 = Random.Range(0,1);
        int rand3 = Random.Range(0,1);
        float rand4 = Random.Range(0,360);
        newParticle.transform.RotateAround(puzzleObject.transform.position,new Vector3(rand1,rand2,rand3),rand4);
        particles.Add(newParticle);
    }

    protected override void Update()
    {
        base.Update();
        if (base.isPlayingPuzzle)
        {
            if (amountOfParticles == 0)
            {
                FinishedPuzzle();
            }
        }
    }

    IEnumerator CheckShake()
    {
        while (base.isPlayingPuzzle)
        {
            // MAYBE TRY SUDDEN CHANGES IN DIRECTION
            var dragDistance = (lastPosition - puzzleObject.transform.position).magnitude;
            if (dragDistance > effectDistance)
            {
                // Remove one particle on the item.
                RemoveParticle();
            }
            lastPosition = puzzleObject.transform.position;
            yield return checkingInterval;
        }

        yield break;
    }

    private void RemoveParticle()
    {
        var randomInt = Random.Range(0,particles.Count);
        var dir = puzzleObject.transform.position - lastPosition;
        particles[randomInt].transform.Translate(dir * Time.deltaTime, Space.World);

        Destroy(particles[randomInt]);
        particles.RemoveAt(randomInt);
        amountOfParticles--;
    }

    public override void StartPuzzle()
    {
        base.StartPuzzle();
        puzzleObject.SetActive(true);
        // Set up distance between camera and puzzleObject, for the dragging function to work.
        puzzleObject.GetComponent<DragObject>().SetDistance();
        StartCoroutine("CheckShake");
    }

    protected override void FinishedPuzzle()
    {
        base.FinishedPuzzle();
    }

    protected override void LostPuzzle()
    {
        base.LostPuzzle();
    }

}
