﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoicePuzzle : Puzzle
{
    private List<GameObject> spawnedObjects = new List<GameObject>();
    public int amountToSpawn = 3;
    private int amountPopped = 0;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
        
        if (base.isPlayingPuzzle)
        {
            // Check for input on top of any of the spawned objects.
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray,out hit))
                {
                    for(int a = 0; a < spawnedObjects.Count; a++)
                    {
                        if (hit.collider && hit.collider == spawnedObjects[a].GetComponent<Collider>())
                        {
                            amountPopped++;
                            hit.collider.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }
        
        if (amountPopped == amountToSpawn && base.isPlayingPuzzle)
        {
            FinishedPuzzle();
        }
    }

    public override void StartPuzzle()
    {
        base.StartPuzzle();

        for (int j = 0; j < amountToSpawn; j++)
        {
            var newObject = Instantiate(FindObjectOfType<PuzzleManager>().MinigameObjects[0]);
            // Change the position.
            newObject.transform.position = transform.position + Vector3.up + (0.4f * (j-(Mathf.Floor(amountToSpawn / 2))) * Camera.main.transform.right);
            newObject.transform.parent = this.transform;
            spawnedObjects.Add(newObject);
        }
    }

    protected override void FinishedPuzzle()
    {
        base.FinishedPuzzle();
    }

    protected override void LostPuzzle()
    {
        base.LostPuzzle();
        
        for (int i = 0; i < spawnedObjects.Count; i++)
        {
            spawnedObjects[i].SetActive(false);
        }
    }
}
