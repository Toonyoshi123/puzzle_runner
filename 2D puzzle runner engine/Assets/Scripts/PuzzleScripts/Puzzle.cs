﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    /// <summary>
    /// The path this puzzle is attached to.
    /// </summary>
    public BezierCurve path;
    private FollowPath playerFollow;
    private CameraFollow cam;
    private PuzzleManager puzzleManager;
    /// <summary>
    /// The t percentage of the path that this puzzle is positioned at.
    /// </summary>
    public float pathPosition { get; set; } = 0;
    public float maxDuration { get; set; } = 10;
    /// <summary>
    /// The position of the player, relative to the puzzle, when the player reaches the puzzle.
    /// </summary>
    public float playerPosition { get; private set; } = 0.01f;
    private float duration = 0f;
    public bool isPlayingPuzzle { get; set; } = false;
    public bool finishedThisPuzzle { get; set; } = false;

    protected virtual void Start()
    {
        playerFollow = FindObjectOfType<FollowPath>();
        cam = FindObjectOfType<CameraFollow>();
        puzzleManager = FindObjectOfType<PuzzleManager>();
    }

    protected virtual void Update()
    {
        if (isPlayingPuzzle)
        {
            duration += Time.deltaTime;
        }
        if (duration >= maxDuration && isPlayingPuzzle)
        {
            isPlayingPuzzle = false;
            LostPuzzle();
        }
    }

    /// <summary>
    /// The player encountered a puzzle!
    /// </summary>
    public virtual void StartPuzzle()
    {
        Debug.Log("Starting Puzzle!");
        // Stop the moving object.
        puzzleManager.isInPuzzle = true;
        duration = 0;
        isPlayingPuzzle = true;
        cam.IsDoingPuzzle = true;
        playerFollow.pathpercentage = pathPosition - playerPosition;
        playerFollow.CanMove = false;
    }

    /// <summary>
    /// The player finished the level!
    /// Continue onward!
    /// </summary>
    protected virtual void FinishedPuzzle()
    {
        Debug.Log("Finished Puzzle!");
        // Let the stopped object move again.
        puzzleManager.isInPuzzle = false;
        isPlayingPuzzle = false;
        cam.IsDoingPuzzle = false;
        playerFollow.pathpercentage = pathPosition - playerPosition;
        puzzleManager.puzzles.Remove(this);
        playerFollow.CanMove = true;
        finishedThisPuzzle = true;
        FindObjectOfType<TimerScript>().isRunning = false;
        FindObjectOfType<Rewards>().IncreaseRewardCount();
        // Destory this puzzle, and every child object on it. To save memory.
        Destroy(this.gameObject);
    }

    /// <summary>
    /// The player lost the puzzle by taking too long...
    /// They are returned to the main menu.
    /// </summary>
    protected virtual void LostPuzzle()
    {
        Debug.Log("Lost puzzle!");
        FindObjectOfType<PathManager>().hasMadeMistakes = true;
        FindObjectOfType<DifficultyManager>().ChangeLevel(false);

        FindObjectOfType<TimerScript>().isRunning = false;
        // Stop this level and move back to main menu.
        puzzleManager.isInPuzzle = false;
        isPlayingPuzzle = false;
        cam.IsDoingPuzzle = false;
        playerFollow.pathpercentage = pathPosition - playerPosition;
        puzzleManager.puzzles.Remove(this);
        playerFollow.CanMove = true;
        finishedThisPuzzle = true;
        Destroy(this.gameObject);
    }
}
