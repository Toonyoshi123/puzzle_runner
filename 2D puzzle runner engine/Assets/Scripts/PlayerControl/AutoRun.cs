﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRun : MonoBehaviour
{
    private FollowPath player;
    private Animator anim;
    [SerializeField]
    [Range(5,15)]
    float speed = 10;
    public float pathLength { get; set; } = 0f;

    void Start()
    {
        player = FindObjectOfType<FollowPath>();
        anim = GetComponentInChildren<Animator>();
    }
    
    void Update()
    {
        if (player.CanMove)
        {
            // If the player is not already playing the looping Run animation, start playing it.
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Running"))
            {
                anim.Play("Running");
                anim.SetBool("IsRunning", true);
            }
            // The percentage per step here, is to try and keep a steady speed across different sizes of paths.
            // The original size was 2.008 units long, so that is the base to work from. NOT PERFECT YET THOUGH.
            var percentagePerStep = 0.01f / (pathLength / 2.008f);
            player.pathpercentage = Mathf.Lerp(player.pathpercentage, player.pathpercentage + (percentagePerStep * speed), Time.deltaTime);
        }
        else{
            // If the player is not already playing the looping Idle animation, start playing it.
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                anim.Play("Idle");
                anim.SetBool("IsRunning", false);
            }
        }
    }
}
