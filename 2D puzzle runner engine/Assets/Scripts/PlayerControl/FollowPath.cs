﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AutoRun))]
[RequireComponent(typeof(PathManager))]

public class FollowPath : MonoBehaviour
{

    /// <summary>
    /// The path for this object to follow in the x and z directions.
    /// </summary>
    public BezierCurve path;
    /// <summary>
    /// A percentage between 0 and 1. This determines where in the entirety of the path this object will be.
    /// </summary>
    public float pathpercentage { get; set; } = 0f;
    /// <summary>
    /// This boolean is to make sure this object does not move when the cross-roads appear.
    /// Only turn this off after visibly stopping.
    /// </summary>
    public bool CanMove { get; set; } = true;
    /// <summary>
    /// For the player movement, if possibly invertible, returns false for left, true for right.
    /// </summary>
    public bool leftOrRight { get; set; } = true;

    private void Start()
    {
        path = FindObjectOfType<PathManager>().path;
        pathpercentage = 0;
        followPath();
    }

    private void Update()
    {
        followPath();
        pathpercentage = Mathf.Clamp(pathpercentage,0.00f,0.99f);
    }

    /// <summary>
    /// Makes the object this is attached to follow a path created in the editor.
    /// This includes a change in rotation.
    /// </summary>
    private void followPath()
    {
        if (CanMove)
        {
            var pointA = path.GetPointAt(pathpercentage);
            Vector3 lookDirection = new Vector3();
            if (pathpercentage < 0.99f)
            {
                lookDirection = path.GetPointAt(pathpercentage + 0.01f);
            }
            lookDirection.y = transform.position.y; // Can add a variable here for slanted platforms.
            transform.LookAt(lookDirection);

            // Move up to the percentage of the total length.
            transform.position = new Vector3(pointA.x,transform.position.y,pointA.z);
        }
    }
}
