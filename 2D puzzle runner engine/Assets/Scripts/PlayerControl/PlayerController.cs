﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public BezierCurve path;
    private FollowPath player;
    private Rigidbody rig;
    private CameraFollow cam;
    private Animator anim;

    [SerializeField]
    [Range(1, 10)]
    private float jumpVelocity = 1;
    [SerializeField]
    [Range(1, 10)]
    private float fallMultiplier = 2.5f;
    [SerializeField]
    [Range(1, 10)]
    private float lowJumpMultiplier = 2f;
    [SerializeField]
    [Range(1, 10)]
    private float horizontalSpeed = 5;
    private float speed = 0;
    [SerializeField]
    [Range(10, 30)]
    private float maxSpeed = 15;
    public float pathLength { get; set; } = 0f;

    private bool isJumping = false;
    public bool isGrounded = false;

    private void Start()
    {
        player = GetComponent<FollowPath>();
        rig = GetComponent<Rigidbody>();
        cam = FindObjectOfType<CameraFollow>();
        anim = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (player.CanMove)
        {
            if (Input.GetKeyDown(KeyCode.Space) && !isJumping && isGrounded)
            {
                Jump();
            }
            if (!GetComponent<AutoRun>().isActiveAndEnabled) {
                if (Input.GetAxis("Horizontal") > 0)
                {
                    Move(true);
                }
                else if (Input.GetAxis("Horizontal") < 0)
                {
                    Move(false);
                }
                else
                {
                    speed = 0;
                    anim.SetBool("IsRunning", false);
                    anim.Play("Idle");
                }
            }

            // To prevent the player from falling through the terrain/terrain model.
            Ray ray = new Ray(transform.position,Vector3.down);
            RaycastHit hit;
            if (!Physics.Raycast(ray, out hit))
            {
                var currentposition = transform.position;
                currentposition.y = .2f;
                transform.position = currentposition;
            }

            #region verticalMovement
            // Check if gravity should be applied.
            if (!isGrounded)
            {
                if (rig.velocity.y < 0)
                {
                    rig.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
                }
                else if (rig.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
                {
                    rig.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
                }
            }

            #endregion
            // percentagePerStep is to make sure the visual speed remains the same across different sizes of paths.
            // 2.008 was the original size that the code was based on, so it is the basis here. NOT PERFECT YET.
            var percentagePerStep = 0.01f / (pathLength / 2.008f);
            player.pathpercentage = Mathf.Lerp(player.pathpercentage, player.pathpercentage + (percentagePerStep * speed), Time.deltaTime);
        }

    }

    private void Jump()
    {
        Debug.Log("Jump");
        anim.SetBool("IsJumping",true); 
        anim.Play("Jump");
        isGrounded = false;
        rig.velocity = Vector3.up * jumpVelocity;
        isJumping = true;
    }

    private void Move(bool movesRight)
    {
        // To keep the player moving the right direction based on Input.
        if (cam.isOnRightSide)
        {
            speed += ((movesRight) ? horizontalSpeed : -horizontalSpeed);
        }
        else
        {
            speed += ((movesRight) ? -horizontalSpeed : horizontalSpeed);
        }
        speed = Mathf.Clamp(speed,-maxSpeed,maxSpeed);
        anim.SetBool("IsRunning", true);
        // Return animation to running when on the ground.
        if (isGrounded && !anim.GetCurrentAnimatorStateInfo(0).IsName("Running"))
        {
            anim.Play("Running");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
        anim.SetBool("IsJumping", false);
        isJumping = false;
    }
}
