﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private FollowPath player;
    public BezierCurve path;
    private LookAtPuzzle lookAt;

    /// <summary>
    /// Whather or not the camera is on the right side of the player character.
    /// </summary>
    [SerializeField]
    public bool isOnRightSide { get; private set; } = true;
    public bool IsDoingPuzzle { get; set; } = false;

    /// <summary>
    /// The position on the path, that the camera is at.
    /// </summary>
    public float cameraPathPosition { get; set; } = 0;
    [SerializeField]
    [Range(10, 20)]
    private float cameraSpeed = 10;
    /// <summary>
    /// The set distance the camera should keep from the player.
    /// </summary>
    [SerializeField]
    [Range(.1f,2)]
    private float distanceFromPlayer = 2f;
    /// <summary>
    /// The max left and right boundaries to keep the player inside of.
    /// </summary>
    [SerializeField]
    [Range(0, 0.2f)]
    private float xBounds = 0f;
    /// <summary>
    /// The max top and bottom boundaries to keep the player inside of.
    /// </summary>
    [SerializeField]
    [Range(0, 0.2f)]
    private float yBounds = 0f;

    private void Start()
    {
        player = FindObjectOfType<FollowPath>();
        path = FindObjectOfType<PathManager>().path;
        lookAt = FindObjectOfType<LookAtPuzzle>();
    }

    private void Update()
    {
        cameraPathPosition = Mathf.Clamp(cameraPathPosition, 0.01f, 0.99f);
        var playerScreenPosition = Camera.main.WorldToScreenPoint(player.transform.position);
        if (playerScreenPosition.x <= Screen.width *(0.5f - xBounds) || playerScreenPosition.x >= Screen.width * (0.5f + xBounds) ||
            playerScreenPosition.y <= Screen.height * (0.5f - yBounds) || playerScreenPosition.y >= Screen.height * (0.5f + yBounds))
        {
            if (player.CanMove)
            {
                FollowPlayer();
            }
            else if(IsDoingPuzzle)
            {
                lookAt.lookAtPuzzle();
            }
        }
    }

    /// <summary>
    /// The camera follows the player at a set distance. This can be different for each scene.
    /// Depending on the boundaries, the camera will only follow the player if they are outside the boundaries,
    /// </summary>
    private void FollowPlayer()
    {
        // Keeps the player within boundaries of the screen.
        // Keeps set distance.
        var speed = (Mathf.Abs(player.pathpercentage - cameraPathPosition)) * 100 * cameraSpeed;
        Vector3 perp = new Vector3();
        if (isOnRightSide)
        {
            // These vectors only work for a camera on the Player's right side.
            player.leftOrRight = true;
            Vector3 pathVector = path.GetPointAt(cameraPathPosition - 0.01f) - 
                (path.GetPointAt(cameraPathPosition + ((cameraPathPosition < 0.99f) ? 0.01f:0)));
            perp = Vector3.Cross(pathVector, Vector3.up).normalized;
        }
        else
        {
            // These vectors only work for a camera on the Player's left side.
            player.leftOrRight = false;
            Vector3 pathVector = path.GetPointAt(cameraPathPosition + 0.01f) - 
                (path.GetPointAt(cameraPathPosition - ((cameraPathPosition > 0.01f) ? 0.01f:0)));
            perp = Vector3.Cross(pathVector, Vector3.up).normalized;
        }

        // The y axis needs to be updatable based on the player's y position.
        var height = player.transform.position.y + .2f;
        height = Mathf.Clamp(height,0,Mathf.Infinity);
        perp.y = 0;

        // Adjust the camera position.
        Vector3 lookDirection = path.GetPointAt(cameraPathPosition);
        lookDirection.y = height;
        Vector3 newLocation = lookDirection + perp * distanceFromPlayer;
        transform.position = Vector3.Slerp(transform.position, newLocation,Time.deltaTime);
        // Adjust the rotation.
        var rotation = lookDirection - transform.position;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(rotation),Time.deltaTime *2);

        // Adjust the camera's position compared to the path.
        cameraPathPosition = Mathf.Lerp(cameraPathPosition, 
            cameraPathPosition + (0.01f * speed) * ((player.pathpercentage > cameraPathPosition) ?1:-1), 
            Time.deltaTime);
    }
}
