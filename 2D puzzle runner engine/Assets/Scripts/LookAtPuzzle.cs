﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPuzzle : MonoBehaviour
{
    private PuzzleManager manager;

    void Start()
    {
        manager = FindObjectOfType<PuzzleManager>();
    }

    /// <summary>
    /// The camera takes a set position and rotation for the puzzle.
    /// </summary>
    public void lookAtPuzzle()
    {
        if (transform.position != manager.currentCameraPos || transform.rotation != manager.currentCameraRot)
        {
            transform.position = Vector3.Slerp(transform.position, manager.currentCameraPos, Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, manager.currentCameraRot, Time.deltaTime);
        }
    }
}
