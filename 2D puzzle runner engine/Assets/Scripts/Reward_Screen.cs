﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Reward_Screen : MonoBehaviour
{
    public int highestDifficulty = 0;
    public int currentTotalScore = 0;
    public int levelsCleared = 0;

    [SerializeField]
    private TextMeshProUGUI totalScoreText;
    [SerializeField]
    private TextMeshProUGUI levelsClearedText;
    [SerializeField]
    private TextMeshProUGUI highestDifficultyText;
    private WorldManager manager;

    private void Start()
    {
        // Update variables from the undestroyed object,
        // And display them.
        manager = FindObjectOfType<WorldManager>();
        var managerTracker = manager.GetComponent<Reward_Screen>();

        levelsCleared = managerTracker.levelsCleared;
        currentTotalScore = managerTracker.currentTotalScore;
        highestDifficulty = managerTracker.highestDifficulty;
        if (this.gameObject.name == "Rewards_Navigation_And_Tracker")
        {
            DisplayScores();
        }
    }

    private void DisplayScores()
    {
        levelsClearedText.text = "Levels Cleared: " + levelsCleared;
        totalScoreText.text = "X " + currentTotalScore;
        string difficulty = "";
        if (highestDifficulty == 0)
        {
            difficulty = "Easy";
        }
        else if (highestDifficulty == 1)
        {
            difficulty = "Normal";
        }
        else
        {
            difficulty = "Hard";
        }
        highestDifficultyText.text = "Highest difficulty reached:       " + difficulty;
    }

    public void GoToHome()
    {
        SceneManager.LoadScene(0);
    }

    public void GoToRewards()
    {
        SceneManager.LoadScene("Rewards");
    }
}
