﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    float distance;

    public void SetDistance()
    {
        distance = (transform.position - Camera.main.transform.position).magnitude;
    }

    private void OnMouseDrag()
    {
        // Move this object to the world mouse position.
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
    }
}
