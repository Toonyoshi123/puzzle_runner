﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public BezierCurve path;
    private FollowPath player;
    private TimerScript timer;
    [SerializeField]
    private GameObject puzzleIndicator;
    public List<Puzzle> puzzles = new List<Puzzle>();
    [SerializeField]
    private List<Puzzle> puzzleVariants = new List<Puzzle>();
    public List<GameObject> MinigameObjects = new List<GameObject>();

    [Range(1, 3)]
    public int amountOfPuzzles = 1;
    public int puzzleInt { get; set; } = 0;

    public bool isInPuzzle { get; set; } = false;

    public Vector3 currentCameraPos { get; private set; } = new Vector3();
    public Quaternion currentCameraRot { get; private set; } = new Quaternion();

    private void Start()
    {
        path = FindObjectOfType<PathManager>().path;
        player = FindObjectOfType<FollowPath>();
        timer = FindObjectOfType<TimerScript>();
        PlacePuzzles();
    }

    private void Update()
    {
        if (puzzles.Count != 0 && !puzzles[puzzleInt].finishedThisPuzzle && 
            player.pathpercentage >= (puzzles[puzzleInt].pathPosition - puzzles[puzzleInt].playerPosition) && 
            !isInPuzzle && puzzles[puzzleInt].GetComponent<Puzzle>().path == this.path)
        {
            // Send the camera to a position, based on the perpendicular line to the path.
            Vector3 pathVector = path.GetPointAt(puzzles[puzzleInt].pathPosition - 0.01f) - path.GetPointAt(puzzles[puzzleInt].pathPosition + 0.01f);
            Vector3 perp = Vector3.Cross(pathVector, Vector3.up).normalized;
            perp.y = 0;
            currentCameraPos = path.GetPointAt(puzzles[puzzleInt].pathPosition) + (perp * 2f) + (Vector3.up);
            currentCameraRot = Quaternion.LookRotation(path.GetPointAt(puzzles[puzzleInt].pathPosition) - currentCameraPos + (Vector3.up* .5f));
            puzzles[puzzleInt].StartPuzzle();
            timer.StartTimer(puzzles[puzzleInt].maxDuration);
        }
    }

    /// <summary>
    /// Places [amountOfPuzzles] puzzles on the current path, with randomized type of puzzle.
    /// </summary>
    public void PlacePuzzles()
    {
        for (int i = 0; i < amountOfPuzzles; i++)
        {
            var nextPuzzle = Instantiate(puzzleIndicator);
            int randomInt = Mathf.RoundToInt(Random.Range(0,puzzleVariants.Count));
            // Add puzzle variant's script component.
            System.Type newcomponentType = puzzleVariants[randomInt].GetType();
            nextPuzzle.AddComponent(newcomponentType);
            // Apply the necessary variables.
            var nextPuzzlePuzzle = nextPuzzle.GetComponent<Puzzle>();
            nextPuzzlePuzzle.path = path;
            nextPuzzlePuzzle.pathPosition = (1f / ((float)amountOfPuzzles + 1f)) * (i+1);
            nextPuzzle.transform.position = path.GetPointAt(1f / ((float)amountOfPuzzles + 1f) * (i+1));
            FindObjectOfType<DifficultyManager>().InsertVariables(newcomponentType, nextPuzzle);
            puzzles.Add(nextPuzzlePuzzle);
        }
    }
}
