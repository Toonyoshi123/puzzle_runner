﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathManager : MonoBehaviour
{
    [SerializeField]
    public BezierCurve path;
    [SerializeField]
    private Slider progress;
    [SerializeField]
    private List<BezierCurve> otherPaths = new List<BezierCurve>();
    private FollowPath follow;
    [SerializeField]
    private GameObject left;
    [SerializeField]
    private GameObject right;
    [SerializeField]
    private List<int> totalPathsPerIntersection = new List<int>();
    private BezierCurve nextPath;

    private WorldManager worldManager;
    private DifficultyManager difficultyManager;
    private PuzzleManager puzzleManager;
    private CameraFollow cameraFollow;

    private int amountOfPathsPassed = 0;
    private int amountOfPathsToPass;

    public bool hasMadeMistakes = false;
    bool hasFinished = false;

    void Start()
    {
        worldManager = FindObjectOfType<WorldManager>();
        puzzleManager = FindObjectOfType<PuzzleManager>();
        cameraFollow = FindObjectOfType<CameraFollow>();
        difficultyManager = FindObjectOfType<DifficultyManager>();
        follow = GetComponent<FollowPath>();
        amountOfPathsToPass = totalPathsPerIntersection.Count;
        progress.maxValue = 1 + amountOfPathsToPass;
        nextPath = path;
    }

    void Update()
    {
        // Show choices a little while before the intersection.
        if (follow.pathpercentage > 0.8f && otherPaths.Count > 1 && nextPath == path && totalPathsPerIntersection.Count > amountOfPathsPassed)
        {
            if (totalPathsPerIntersection[amountOfPathsPassed] > 1)
            {
                left.SetActive(true);
                right.SetActive(true);
            }
        }
        // Chnage the path to the chosen path. If none are chosen, waits until one is chosen.
        if (follow.pathpercentage >= 0.99f && nextPath != path)
        {
            ChangePath(nextPath);
            left.SetActive(false);
            right.SetActive(false);
        }
        else if(follow.pathpercentage >= 0.99f && nextPath == path)
        {
            follow.CanMove = false;
        }
        // If there is no path to choose, but still a path to follow, go to this next path.
        if (amountOfPathsPassed < totalPathsPerIntersection.Count && totalPathsPerIntersection[amountOfPathsPassed] == 1)
        {
            nextPath = otherPaths[0];
        }

        if (amountOfPathsPassed == amountOfPathsToPass && follow.pathpercentage >= 0.99f && !hasFinished)
        {
            hasFinished = true;
            worldManager.FinishedLevel();
        }

        DisplayProgress();
    }

    private void DisplayProgress()
    {
        progress.value = amountOfPathsPassed + follow.pathpercentage;
    }

    public void ChangePathByButton(bool isLeft)
    {
        if (isLeft)
        {
            nextPath = otherPaths[0];
        }
        else
        {
            nextPath = otherPaths[1];
        }
        // Will need to check if any puzzle has been lost...
        if (!hasMadeMistakes)
        {
            difficultyManager.ChangeLevel(true);
        }
        else
        {
            hasMadeMistakes = false;
        }
    }

    /// <summary>
    /// Changes the path this object is following. To make this work, place new paths at the
    /// cross-point. After/during the switch, set percentage back to 0.
    /// </summary>
    /// <param name="newPath"> The chosen path to follow next. </param>
    public void ChangePath(BezierCurve newPath)
    {
        RemovePathChoices(totalPathsPerIntersection[amountOfPathsPassed]);
        amountOfPathsPassed++;
        follow.path = newPath;
        follow.pathpercentage -= 1;
        path = newPath;
        cameraFollow.path = newPath;
        cameraFollow.cameraPathPosition = 0;
        puzzleManager.path = newPath;
        puzzleManager.puzzles.Clear();
        puzzleManager.PlacePuzzles();
        follow.CanMove = true;
    }

    public void RemovePathChoices(int totalToDestroy)
    {
        for (int i = 0; i < totalToDestroy; i++)
        {
            otherPaths.RemoveAt(0);
        }
    }
}
