﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WorldManager : MonoBehaviour
{
    [SerializeField]
    private GameObject indicator;
    private GameObject[] worlds;
    private Reward_Screen rewardsTracker;
    private Rewards rewards;
    private DifficultyManager difficultyManager;

    public List<Levels> levels = new List<Levels>();
    public int level = 0;
    public Levels currentlyPlayedLevel;

    /// <summary>
    /// Variable to move the base height of the level indicator.
    /// </summary>
    [SerializeField]
    private float heightOffsetIndicator = 150;

    // Use 2 for Park world.
    private List<bool> occupiedWorlds = new List<bool>();

    private void Awake()
    {
        rewardsTracker = GetComponent<Reward_Screen>();

        // Loads current save file, if one exists.
        DataSaver saveData = SaveSystem.Load();
        if (saveData != null)
        {
            level = saveData.level;
            for (int i = 0; i < levels.Count; i++)
            {
                levels[i].hasBeenFinished = saveData.finishedLevels[i];
            }

            rewardsTracker.highestDifficulty = saveData.highestDifficulty;
            rewardsTracker.levelsCleared = saveData.levelsFinished;
            rewardsTracker.currentTotalScore = saveData.totalScore;
        }
    }

    private void Start()
    {
        worlds = GameObject.FindGameObjectsWithTag("World");
        for (int i = 0; i < worlds.Length; i++)
        {
            occupiedWorlds.Add(false);
        }
    }

    public void NextLevel()
    {
        worlds = GameObject.FindGameObjectsWithTag("World");
        for (int i = 0; i < worlds.Length; i++)
        {
            occupiedWorlds[i] = false;
        }
        // Based on leveling progress, new missions/levels appear...
        foreach (Levels lvl in levels)
        {
            if (lvl.level <= level && !lvl.hasBeenFinished)
            {
                // Show level on location, if location is empty.
                if (occupiedWorlds[lvl.worldInt] == false)
                {
                    ShowLevel(lvl);
                }
            }
        }
    }

    private Vector3 LocateWorldPosition(int world)
    {
        var location = Camera.main.WorldToScreenPoint(worlds[world].transform.position);
        location.y += heightOffsetIndicator;
        location.z = 0;
        return location;
    }

    private void ShowLevel(Levels newLevel)
    {
        var newIndicator = Instantiate(indicator, this.transform);
        newIndicator.GetComponent<Image>().sprite = newLevel.levelImage;;
        newIndicator.GetComponent<RectTransform>().position = LocateWorldPosition(newLevel.worldInt);
        newIndicator.GetComponentInChildren<Level_Select_Button>().levelNumber = newLevel.SceneInt;
        newIndicator.GetComponentInChildren<Level_Select_Button>().level = newLevel;
        occupiedWorlds[newLevel.worldInt] = true;
    }

    public void FinishedLevel()
    {
        difficultyManager = FindObjectOfType<DifficultyManager>();
        rewards = FindObjectOfType<Rewards>();
        level++;
        rewardsTracker.highestDifficulty = difficultyManager.highestLevel;
        rewardsTracker.levelsCleared++;
        rewardsTracker.currentTotalScore += rewards.rewardCount;

        currentlyPlayedLevel.hasBeenFinished = true;
        levels[levels.IndexOf(currentlyPlayedLevel)].hasBeenFinished = true;
        // Load world map again
        SceneManager.LoadScene("Rewards");
        SaveSystem.Save(this, rewardsTracker);
        GetComponent<Canvas>().enabled = true;
    }

    /// <summary>
    /// To be used to completely reset the current progress. VERY CAREFUL!
    /// </summary>
    public void ResetProgress()
    {
        SaveSystem.Delete();
        foreach (Levels lvl in levels)
        {
            lvl.hasBeenFinished = false;
        }

        level = 0;
        currentlyPlayedLevel = null;

        rewardsTracker.highestDifficulty = 0;
        rewardsTracker.levelsCleared = 0;
        rewardsTracker.currentTotalScore = 0;

        SaveSystem.Save(this, rewardsTracker);
    }
}
