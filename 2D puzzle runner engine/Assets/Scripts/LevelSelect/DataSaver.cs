﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataSaver
{
    public int level;
    public bool[] finishedLevels;
    // For the rewards.
    public int levelsFinished;
    public int totalScore;
    public int highestDifficulty;

    public DataSaver(WorldManager manager, Reward_Screen trackedRewards)
    {
        level = manager.level;
        finishedLevels = new bool[manager.levels.Count];
        for (int i = 0; i < finishedLevels.Length; i++)
        {
            finishedLevels.SetValue(manager.levels[i].hasBeenFinished,i);
        }

        levelsFinished = trackedRewards.levelsCleared;
        totalScore = trackedRewards.currentTotalScore;
        highestDifficulty = trackedRewards.highestDifficulty;
    }
}
