﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigator : MonoBehaviour
{
    private WorldManager manager;

    private void Start()
    {
        manager = FindObjectOfType<WorldManager>();
    }

    public void ResetProgressButton()
    {
        manager.ResetProgress();
    }

    public void StartButton()
    {
        manager.enabled = true;
        manager.NextLevel();
    }

    public void EndGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
