﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void Save(WorldManager manager, Reward_Screen rewardsTracker)
    {
        string pathName = Application.persistentDataPath + "/gameData.save";
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(pathName,FileMode.Create);
        DataSaver data = new DataSaver(manager,rewardsTracker);
        formatter.Serialize(stream,data);
        stream.Close();
        Debug.Log("Saved the current game-data!");
    }

    public static DataSaver Load()
    {
        string pathName = Application.persistentDataPath + "/gameData.save";
        if (File.Exists(pathName))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(pathName,FileMode.Open);
            DataSaver data = formatter.Deserialize(stream) as DataSaver;
            stream.Close();
            Debug.Log("Loaded existing Data!");
            return data;
        }
        else
        {
            Debug.Log("There was no data to load!");
            return null;
        }
    }

    public static void Delete()
    {
        string pathName = Application.persistentDataPath + "/gameData.save";
        if (File.Exists(pathName))
        {
            File.Delete(pathName);
            Debug.Log("Deleted existing file!");
        }
        else
        {
            Debug.Log("There was no file to delete!");
        }
    }
}
