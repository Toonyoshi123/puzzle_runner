﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level_Select_Button : MonoBehaviour
{
    private WorldManager worldManager;

    public int levelNumber = 0;
    public Levels level;

    private void Start()
    {
        worldManager = FindObjectOfType<WorldManager>();
    }

    public void MoveToWorld()
    {
        // Change currentLevel
        var indicators = worldManager.GetComponentsInChildren<CanvasRenderer>();
        foreach (var indicator in indicators)
        {
            Destroy(indicator.gameObject);
        }
        worldManager.currentlyPlayedLevel = level;
        worldManager.GetComponent<Canvas>().enabled = false;
        SceneManager.LoadScene(levelNumber);
    }
}
