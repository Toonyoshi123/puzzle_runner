﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LevelSelect/Level")]
public class Levels : ScriptableObject
{
    public int SceneInt;
    public Vector2 mapLocation;
    public Sprite levelImage;
    public int level;
    public List<Sprite> rewards = new List<Sprite>();
    public bool hasBeenFinished = false;
    public int difficulty = 0;
    public int worldInt = 0;

    // It would be possible to, instead of randomly making a list of puzzles,
    // make the list in here... Or percentages for how often a puzzle shows up.
}
