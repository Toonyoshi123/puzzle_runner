﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class UI_Volume_Slider : MonoBehaviour
{
    [SerializeField]
    private AudioMixer mixer;
    [SerializeField]
    private Slider slider;

    public void SetLevel()
    {
        float sliderValue = slider.value;

        if (this.gameObject.name == "Music_Volume_Slider")
        {
            mixer.SetFloat("Volume_Music",Mathf.Log10(sliderValue) * 20);
        }
        else if (this.gameObject.name == "SFX_Volume_Slider")
        {
            mixer.SetFloat("Volume_SFX", Mathf.Log10(sliderValue) * 20);
        }
    }
}
